# Sysctl configuration management

This role implements configuration management for sysctl. It will remove all configuration files and parameters and deploy the corrcet configuration(s) using ansible.

# Features
- Configuration management for sysctl parameters
- All others parameters will be removed
- The parameters will be reloaded on change
- There are some templates with sane defaults for different purposes

Two templates are enabled by default:
* protect-links.conf
* disable-swap.conf

# Example usage
```
- name: "Deploy sysctl templates"
  hosts: all
  become: true
  roles:
    - role: sysctl-configuration-management
      sysctl_enabled_templates:
       - high-connections.conf
       - disable-ipv6.conf
       - high-no-processes.conf 
       - large-directory-trees.conf
       - open-files-limits.conf
       - high-unstable-connections.conf
      sysctl_extra_parameters:
       - parameter: net.ipv4.ip_forward
         value: 1
         comment: We want to forward some IP packets from this machine

- name: "Remove all sysctl configuration"
  hosts: somehost.somedomain
  become: true
  roles:
    - role: sysctl-configuration-management
      sysctl_enabled_templates: []
      sysctl_default_enabled_templates: []
      sysctl_extra_parameters: []
```

# Available templates

Check `templates` folder:
- disable-swap.conf: Allow swapping when all available memory is used.
- high-connections.conf: Improved settings for systems with high (>10k) active tcp connections
- disable-ipv6.conf: Disable IPv6 entirely
- large-directory-trees.conf: Change inotify settings on systems with directories with a large tree (subdirectories + files)
- protect-links.conf: Protect both symbolic links and hard links. This configuration is enabled by default.
- open-files-limit.conf:  Change number of file limits
- high-unstable-connections.conf: Setting for high unstable connections

# Pull requests
Pull requests and extra templates are very welcome. This role is actively maintained.
